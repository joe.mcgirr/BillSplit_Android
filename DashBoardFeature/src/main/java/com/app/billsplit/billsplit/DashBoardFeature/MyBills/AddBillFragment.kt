package com.app.billsplit.billsplit.DashBoardFeature.MyBills

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText

import com.app.billsplit.billsplit.DashBoardFeature.R
import android.widget.Button
import android.widget.Switch
import android.widget.CompoundButton
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.icu.text.DecimalFormat
import android.util.Log
import android.view.KeyEvent
import android.text.Editable




class AddBillFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var mNextButton: Button
    private lateinit var mLinkSwitch: Switch
    private lateinit var mStatementName: TextInputLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    interface OnFragmentInteractionListener {
        fun replaceContentFragment(newFragment: Fragment, title: String)
        fun displayActionBarShadow()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.my_bills_add_bill, container, false)
    }

    @SuppressLint("NewApi")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        listener?.displayActionBarShadow()
        initialiseLinkToBankSwitch()

        val currencyCharacterInput =  view?.findViewById<EditText>(R.id.currency_character)
        currencyCharacterInput?.addTextChangedListener(CurrencyTextWatcher())

        mNextButton = view?.findViewById(R.id.next_button)!!
        mNextButton.setOnClickListener{
            val name: String = view?.findViewById<EditText>(R.id.bill_name_text)?.text.toString()
            val statementName: String? = if(mLinkSwitch.isChecked) view?.findViewById<EditText>(R.id.statement_name_text)?.text.toString() else null
            val costCharacter: String = currencyCharacterInput?.text.toString()
            val costMantissa: String =  view?.findViewById<EditText>(R.id.currency_mantissa)?.text.toString()
            val cost = "$costCharacter.$costMantissa"
            val page2 = AddBillFragmentPage2.newInstance(name, mLinkSwitch.isChecked, statementName, cost)
            listener?.replaceContentFragment(page2,"Add Bill")
        }

    }

    private fun initialiseLinkToBankSwitch(){
        mStatementName = view?.findViewById(R.id.statement_name)!!
        mLinkSwitch = view?.findViewById(R.id.link_to_switch)!!
        mLinkSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            when(isChecked){
                true -> fadeInTextField(mStatementName)
                false -> fadeOutTextField(mStatementName)
            }
        })
    }

    private fun fadeInTextField(field: TextInputLayout){
        field.visibility = View.VISIBLE;
         field.animate()
                 .alpha(1f)
                 .setDuration(800)
                 .setListener(null);
    }

    private fun fadeOutTextField(field: TextInputLayout){
        field.animate()
                .alpha(0f)
                .setDuration(800)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        field.visibility = View.INVISIBLE
                    }
                })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    companion object {
        @JvmStatic
        fun newInstance() = AddBillFragment()
                        /*.apply {
                            arguments = Bundle().apply {
                            }
                        }*/
    }
}
