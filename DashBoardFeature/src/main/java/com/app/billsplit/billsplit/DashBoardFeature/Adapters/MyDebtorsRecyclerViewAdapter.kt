package com.app.billsplit.billsplit.DashBoardFeature.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.AddBillDTO
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.DebtorsListResponse
import com.app.billsplit.billsplit.DashBoardFeature.MyBills.MyDebtorsListFragment
import com.app.billsplit.billsplit.DashBoardFeature.R
import com.github.ivbaranov.mli.MaterialLetterIcon
import kotlinx.android.synthetic.main.my_bills_debtors_list_item.view.*
import java.util.*

class MyDebtorsRecyclerViewAdapter(
        private val mDebtorsResponse: List<DebtorsListResponse>,
        private val mListener: MyDebtorsListFragment.OnListFragmentInteractionListener,
        private val mContext: Context,
        private val bill: AddBillDTO?)
    : RecyclerView.Adapter<MyDebtorsRecyclerViewAdapter.ViewHolder>() {

    private val mOnDebtorClickedListener: View.OnClickListener

    init {
        mOnDebtorClickedListener = View.OnClickListener { v ->
            val debtor = v.tag as DebtorsListResponse
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onDebtorClicked(debtor, bill)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.my_bills_debtors_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mDebtorsResponse[position]
        holder.mDebtorName.text = item.name
        holder.mDebtorNumber.text = item.phoneNo
        holder.mDebtorLetterAvatar.letter = item.name[0].toString()
        holder.mDebtorLetterAvatar.shapeColor = randomColorFromRange()

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnDebtorClickedListener)
        }
    }

    override fun getItemCount(): Int = mDebtorsResponse.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mDebtorName: TextView = mView.debtor_name
        val mDebtorNumber: TextView = mView.phone_number
        val mDebtorLetterAvatar: MaterialLetterIcon = mView.letter_avatar
    }

    fun randomColorFromRange() : Int{
        val androidColors = mContext.getResources().getIntArray(R.array.list_view_avatar_colors)
        val randomAndroidColor = androidColors[Random().nextInt(androidColors.size)]
        return randomAndroidColor
    }
}