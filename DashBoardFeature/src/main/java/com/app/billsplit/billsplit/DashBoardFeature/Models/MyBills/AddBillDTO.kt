package com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills

import java.io.Serializable

data class AddBillDTO(val name: String, val linkedToBank: Boolean, val cost: String, val statementName: String?, val recurrence: Int, val startDate: String, val endDate: String? ) : Serializable