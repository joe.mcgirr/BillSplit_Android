package com.app.billsplit.billsplit.DashBoardFeature.MyBills

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.AddBillDTO
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.DebtorsListResponse

import com.app.billsplit.billsplit.DashBoardFeature.R
import com.warkiz.widget.IndicatorSeekBar
import com.warkiz.widget.OnSeekChangeListener
import android.R.attr.thumbPosition
import com.warkiz.widget.SeekParams



class DebtorHowMuchToPay : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private var debtor: DebtorsListResponse? = null
    private var mBill: AddBillDTO? = null
    private lateinit var mSeekBar: IndicatorSeekBar
    private lateinit var mPercentageIndication: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null){
            debtor = arguments.getSerializable(DEBTOR) as DebtorsListResponse
            mBill = arguments.getSerializable(BILL) as AddBillDTO?
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.mybills_debtor_how_much_to_pay, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.findViewById<TextView>(R.id.seeker_prompt)?.text = "Enter how much of the bill you would like " + debtor?.name + " to pay"
        mPercentageIndication = view?.findViewById(R.id.percentange_indicator)!!
        mSeekBar = view?.findViewById(R.id.seekBar)!!
        mSeekBar.setIndicatorTextFormat("$£{PROGRESS}")
        mSeekBar.setDecimalScale(2)
        mSeekBar.setProgress(mSeekBar.max / 2)
        mPercentageIndication.text = mSeekBar.progress.toString() + "%"
        mSeekBar.onSeekChangeListener = object : OnSeekChangeListener {
            override fun onSeeking(seekParams: SeekParams) {
                val costOfBillAsPercentage = calculatePercentageOfBill(seekParams.progressFloat, mSeekBar.max)
                mPercentageIndication.text = costOfBillAsPercentage.toString() + "%"
            }

            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar) {}

            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {}
        }

    }

    private fun calculatePercentageOfBill(progress: Float, max:Float): Double {
        val percent = (progress / max) * 100
        val percentRounded = Math.round(percent*100.0)/100.0
        return percentRounded
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
    }

    companion object {
        private const val DEBTOR = "selected_debtor"
        private const val BILL = "created_bill"

        @JvmStatic
        fun newInstance(debtor: DebtorsListResponse, bill: AddBillDTO?) =
                DebtorHowMuchToPay().apply {
                    arguments = Bundle().apply {
                        putSerializable(DEBTOR, debtor)
                        putSerializable(BILL, bill)
                    }
                }
    }
}
