package com.app.billsplit.billsplit.DashBoardFeature.MyBills

import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.BillsListResponse
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.DebtorsListResponse
import com.app.billsplit.billsplit.DashBoardFeature.R
import android.support.design.widget.Snackbar
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.AddBillDTO


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MyBills.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MyBills.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MyBillsTabFragment : Fragment(), MyBillsListFragment.OnListFragmentInteractionListener, MyDebtorsListFragment.OnListFragmentInteractionListener {

    private var listener: OnFragmentInteractionListener? = null
    private lateinit var mBillAndDebtorPagerAdapter: BillsAndDebtorsPagerAdapter
    private lateinit var mViewPager: ViewPager
    private lateinit var mTabLayout: TabLayout
    private lateinit var mFabButton: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.my_bills_parent_fragment, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun replaceContentFragment(newFragment: Fragment, title: String)
        fun removeActionBarShadow()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        listener?.removeActionBarShadow()
        mBillAndDebtorPagerAdapter = BillsAndDebtorsPagerAdapter(childFragmentManager, this)
        mViewPager = view!!.findViewById(R.id.pager)
        mViewPager.adapter = mBillAndDebtorPagerAdapter
        mTabLayout = view!!.findViewById(R.id.tab_layout)
        mTabLayout.setupWithViewPager(mViewPager)
        mFabButton = view!!.findViewById(R.id.fab)
        mFabButton.setOnClickListener { view ->
            openAddDebtorOrBill()
        }
    }

    private fun openAddDebtorOrBill(){
        val selectedTab = mTabLayout.selectedTabPosition
        when (selectedTab){
            0 -> listener?.replaceContentFragment(AddBillFragment.newInstance(),"Add Bill")
            1 -> listener?.replaceContentFragment(AddDebtorFragment.newInstance(),"Add Bill")
        }
    }

    override fun onListFragmentInteractionBill(item: BillsListResponse) {
        Log.e("Bill item pressed", "Shliiiiiiiiing")
    }

    override fun onDebtorClicked(item: DebtorsListResponse, bill: AddBillDTO?) {
        Log.e("Debtor item pressed", "Shliiiiiiiiing")
    }

    companion object {
        private const val RESPONSEMODEL_KEY = "response_model"
        //instantiate with api response model
        fun newInstance(): MyBillsTabFragment {
            val fragment = MyBillsTabFragment()
            val bundle = Bundle()
            //bundle.putSerializable(RESPONSEMODEL_KEY, )
            fragment.arguments = bundle

            return fragment
        }
    }


    class BillsAndDebtorsPagerAdapter : FragmentPagerAdapter{
        var mCurrentFragment: MyBillsTabFragment = MyBillsTabFragment()
        val TITLES = arrayOf("Bills ", "Debtors")

        constructor(fm: FragmentManager, currentFragment: MyBillsTabFragment) : super(fm){
            mCurrentFragment = currentFragment
        }
        override fun getCount(): Int {
            return TITLES.size
        }

        override fun getItem(position: Int): Fragment {
            var fragment:Fragment? = null
            when (position){
                0 -> fragment = MyBillsListFragment.newInstance(mCurrentFragment)
                1 -> fragment = MyDebtorsListFragment.newInstance(mCurrentFragment, null)
            }
            return fragment!!
        }

        override fun getPageTitle(position: Int): CharSequence {
            return TITLES[position]
        }

    }
}
