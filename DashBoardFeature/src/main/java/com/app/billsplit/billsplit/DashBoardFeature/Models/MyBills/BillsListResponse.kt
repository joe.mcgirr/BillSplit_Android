package com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills

import org.joda.time.DateTime

data class BillsListResponse(val name: String, val created: DateTime, val cost: Double)