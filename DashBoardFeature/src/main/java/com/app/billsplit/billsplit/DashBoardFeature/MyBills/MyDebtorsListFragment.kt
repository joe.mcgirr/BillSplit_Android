package com.app.billsplit.billsplit.DashBoardFeature.MyBills

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.billsplit.billsplit.DashBoardFeature.Adapters.MyDebtorsRecyclerViewAdapter
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.DebtorsListResponse
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView.VERTICAL
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.AddBillDTO
import com.app.billsplit.billsplit.DashBoardFeature.R


class MyDebtorsListFragment : Fragment() {

    private var mCallBack: OnListFragmentInteractionListener? = null
    private var mBill: AddBillDTO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBill = arguments.getSerializable(BILL) as AddBillDTO?
        //load bundle
    }


    interface OnListFragmentInteractionListener {
        fun onDebtorClicked(debtor: DebtorsListResponse, bill: AddBillDTO?)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.my_bills_debtors_list, container, false)
        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
                adapter = MyDebtorsRecyclerViewAdapter(getDebtorDummyData(), mCallBack!!, context, mBill)
                addItemDecoration(DividerItemDecoration(context, VERTICAL))
            }
        }
        return view
    }

    override fun onDetach() {
        super.onDetach()
        mCallBack = null
    }


    fun getDebtorDummyData(): ArrayList<DebtorsListResponse> {
        return arrayListOf<DebtorsListResponse>(
                DebtorsListResponse("Nuala Flynn", "+44 7583948533", "58e1cfcc-4c5b-4b8e-af9c-4bb3b2a3b942"),
                DebtorsListResponse("Katie Botteril", "+44 7583948533", "41352f1f-8643-47e5-9003-889b6f080a6d"),
                DebtorsListResponse("George Spit", "+44 7583948533", "39f008e1-3dbd-44dd-ba2b-0ae55eb66856"),
                DebtorsListResponse("Natalie Jahadi", "+44 7583948533", "9cead2f0-747b-4573-b2b8-95c56e73bb9b")
        )
    }

    companion object {
        private const val BILL = "created_bill"
        fun newInstance(listener: OnListFragmentInteractionListener, bill: AddBillDTO?): MyDebtorsListFragment =
                MyDebtorsListFragment().apply {
                    if (listener is OnListFragmentInteractionListener) {
                        this.mCallBack = listener
                    } else {
                        throw RuntimeException(listener.toString() + " must implement OnListFragmentInteractionListener")
                    }
                    arguments = Bundle().apply {
                        putSerializable(BILL, bill)
                    }
                }
        }
    }
