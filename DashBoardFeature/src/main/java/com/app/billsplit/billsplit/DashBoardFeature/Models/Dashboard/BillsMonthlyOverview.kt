package com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard

import java.io.Serializable

class BillsMonthlyOverview(billName: String, billId: String, debtorId: String, billCost: Double, totalPaid: Double, billsPayment: ArrayList<BillPayment>, debtorPayment: ArrayList<DebtorPayment>)
                            : BalanceItem(billName, billCost, totalPaid, debtorId, billId), Serializable{
    val billPayments: List<BillPayment> = billsPayment
    val debtorsPayments: List<DebtorPayment> = debtorPayment
}