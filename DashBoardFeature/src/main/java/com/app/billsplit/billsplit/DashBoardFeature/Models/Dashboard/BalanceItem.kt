package com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard

import org.joda.time.DateTime
import java.io.Serializable

open class BalanceItem(name: String, owed: Double, paid: Double, debtorId: String?, billId: String?) : Serializable {
    var name: String = name
    var totalOwed: Double = owed
    var totalPaid: Double = paid
    //empty if bill
    var debtorId: String? = debtorId
    //empty if debtor
    var billId: String? = billId
}