package com.app.billsplit.billsplit.DashBoardFeature.MyBills

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import com.app.billsplit.billsplit.DashBoardFeature.Dashboard.DashboardFragment
import com.app.billsplit.billsplit.DashBoardFeature.Dashboard.NavigationActivity
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.AddBillDTO
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.DebtorsListResponse

import com.app.billsplit.billsplit.DashBoardFeature.R

class NewOrOldDebtor : Fragment(), MyDebtorsListFragment.OnListFragmentInteractionListener {


    private var listener: OnFragmentInteractionListener? = null
    private lateinit var mAddExistingDebtorButton: ImageButton
    private lateinit var mAddNewDebtorButton: ImageButton
    private var existingDebtorClicked: Boolean = false
    private var newDebtorClicked: Boolean = false
    private lateinit var mNextButton: Button
    private lateinit var mSkipButton: Button
    private var mBill: AddBillDTO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mBill = arguments.getSerializable(BILL) as AddBillDTO?
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.my_bills_add_bill_new_or_old_debtor, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun replaceContentFragment(newFragment: Fragment, title: String)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mAddExistingDebtorButton = view?.findViewById(R.id.addExistingDebtor)!!
        mAddNewDebtorButton  = view?.findViewById(R.id.addNewDebtor)!!

        mAddExistingDebtorButton.setOnClickListener{
            createShortToastMessage(resources.getString(R.string.existingDebtorSelectedMessage))
                mAddExistingDebtorButton.background = ContextCompat.getDrawable(context, R.drawable.ic_add_existing_debtor_highlight)
                mAddNewDebtorButton.background = ContextCompat.getDrawable(context, R.drawable.ic_add_new_debtor_accent)
                existingDebtorClicked = true
                newDebtorClicked = false

        }

        mAddNewDebtorButton.setOnClickListener{
            createShortToastMessage(resources.getString(R.string.newDebtorSelectedMessage))
                mAddNewDebtorButton.background = ContextCompat.getDrawable(context, R.drawable.ic_add_new_debtor_highlight)
                mAddExistingDebtorButton.background = ContextCompat.getDrawable(context, R.drawable.ic_add_existing_debtor_accent)
                newDebtorClicked = true
                existingDebtorClicked = false
        }

        initialiseNextButton()

        mSkipButton = view?.findViewById(R.id.skip_button)!!
        mSkipButton.setOnClickListener{
            //load dashboard response model
            val model = NavigationActivity.getDashboardApiResponseModel()
            val dashboardFragment = DashboardFragment.newInstance(model);
            listener?.replaceContentFragment(dashboardFragment, "Monthly Outlook")
        }
    }

    private fun initialiseNextButton(){
        mNextButton = view?.findViewById(R.id.next_button)!!
        mNextButton.setOnClickListener{
            if (newDebtorClicked){

            }
            if (existingDebtorClicked){
                //replace content with debtorsList fragment with this fragment as its interface. This allows us to change what happens
                //when a debtor item is clicked in OnDebtorClicked() override
                val myDebtorsListFrag = MyDebtorsListFragment.newInstance(this, mBill)
                listener?.replaceContentFragment(myDebtorsListFrag, "Select Debtor")
            } else {
                createShortToastMessage(resources.getString(R.string.neitherDebtorTypeSelected))
            }
        }
    }
    override fun onDebtorClicked(item: DebtorsListResponse, bill: AddBillDTO?) {
        //API CALL: Add debtor to existing bill, billId, item.debtorId
        //open debtor percentage, passing in the debtorListResponse
        val debtorToPayFrag = DebtorHowMuchToPay.newInstance(item, bill)
        listener?.replaceContentFragment(debtorToPayFrag, "Add Debtor")
    }

    private fun createShortToastMessage(message: String){
        val toast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
        toast.show()

        val handler = Handler()
        handler.postDelayed(Runnable { toast.cancel() }, 1000)
    }

    companion object {
        private const val BILL = "created_bill"
        @JvmStatic
        fun newInstance(bill: AddBillDTO) =
                NewOrOldDebtor().apply {
                    arguments = Bundle().apply {
                        putSerializable(BILL, bill)
                    }
                }
    }
}
