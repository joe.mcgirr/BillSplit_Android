package com.app.billsplit.billsplit.DashBoardFeature.MyBills

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.AddBillDTO

import com.app.billsplit.billsplit.DashBoardFeature.R
import org.w3c.dom.Text
import java.text.SimpleDateFormat
import java.util.*

class AddBillFragmentPage2 : Fragment() {

    private var listener: OnFragmentInteractionListener? = null
    private lateinit var mStartDateEditText: EditText
    private lateinit var mEndDateEditText: EditText
    private lateinit var mCalander:Calendar
    private lateinit var mReccurenceSpinner: Spinner
    private lateinit var mCreateButton: Button


    //passed by parameter
    var name: String = ""
    var statementName: String? = ""
    var cost = ""
    var linkedToBank: Boolean = false

    interface OnFragmentInteractionListener {
        fun replaceContentFragment(newFragment: Fragment, title: String)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null){
            //get api response model from args
            name = arguments.getSerializable(NAME) as String
            statementName = arguments.getSerializable(STATEMENT) as String?
            cost = arguments.getSerializable(COST) as String
            linkedToBank = arguments.getSerializable(LINKED) as Boolean
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.my_bills_add_bill_2, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mCalander = Calendar.getInstance()
        val dateMessage = view?.findViewById<TextView>(R.id.date_message)
        dateMessage?.text = if(linkedToBank) "Leave the end date blank if you're unsure and we'll alert you when we think the bill has ended"
                            else "Leave the end date blank if you're unsure, you can easily cancel the bill at a later date"

        //add listeners to start and end date fields
        mStartDateEditText = view?.findViewById(R.id.start_date_input)!!
        mStartDateEditText?.setOnClickListener {
            DatePickerDialog(context, getStartDateListener(), mCalander
                    .get(Calendar.YEAR), mCalander.get(Calendar.MONTH),
                    mCalander.get(Calendar.DAY_OF_MONTH)).show()
        }

        mEndDateEditText = view?.findViewById(R.id.end_date_input)!!
        mEndDateEditText?.setOnClickListener {
            DatePickerDialog(context, getEndDateListener(), mCalander
                    .get(Calendar.YEAR), mCalander.get(Calendar.MONTH),
                    mCalander.get(Calendar.DAY_OF_MONTH)).show()
        }

        mReccurenceSpinner = view?.findViewById(R.id.recurrence_dropdown)!!
        ArrayAdapter.createFromResource(
                context,
                R.array.recurrence_types,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            mReccurenceSpinner.adapter = adapter
        }

        mCreateButton = view?.findViewById(R.id.create_button)!!
        mCreateButton.setOnClickListener{
            val recurrence = getReccurenceEnumInteger(mReccurenceSpinner.selectedItemPosition)
            val startDate = mStartDateEditText.text.toString()
            val endDate: String? = mStartDateEditText.text.toString()
            val addBillDTO = AddBillDTO(name, linkedToBank, cost, statementName, recurrence, startDate, endDate)
            //TODO: sendDTO in API request
            listener?.replaceContentFragment(NewOrOldDebtor.newInstance(addBillDTO), "Add Debtor")
        }
    }

    private fun getReccurenceEnumInteger(spinnerPos: Int) : Int{
        val size_values = resources.getStringArray(R.array.recurrence_values)
        val value = Integer.valueOf(size_values[spinnerPos])
        return value
    }

    private fun getStartDateListener() : DatePickerDialog.OnDateSetListener {
        return DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            mCalander.set(Calendar.YEAR, year)
            mCalander.set(Calendar.MONTH, monthOfYear)
            mCalander.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateStartDateLabel()
        }
    }

    private fun getEndDateListener() : DatePickerDialog.OnDateSetListener {
        return DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            mCalander.set(Calendar.YEAR, year)
            mCalander.set(Calendar.MONTH, monthOfYear)
            mCalander.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateEndDateLabel()
        }
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun updateStartDateLabel() {
        val myFormat = "dd/MM/yy"
        val sdf = SimpleDateFormat(myFormat, Locale.UK)

        mStartDateEditText.setText(sdf.format(mCalander.time))
    }

    private fun updateEndDateLabel() {
        val myFormat = "dd/MM/yy"
        val sdf = SimpleDateFormat(myFormat, Locale.UK)

        mEndDateEditText.setText(sdf.format(mCalander.time))
    }

    companion object {
        private const val NAME = "bill_name"
        private const val STATEMENT = "statement_name"
        private const val COST = "bill_cost"
        private const val LINKED = "linked_to_bank"



        @JvmStatic
        fun newInstance(name: String, linkedToBank: Boolean, statementName: String?, cost: String) =
                AddBillFragmentPage2().apply {
                    arguments = Bundle().apply {
                        putSerializable(NAME, name)
                        putSerializable(LINKED, linkedToBank)
                        putSerializable(STATEMENT, statementName)
                        putSerializable(COST, cost)
                    }
                }
    }
}
