package com.app.billsplit.billsplit.DashBoardFeature.Dashboard


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import android.widget.ListView
import com.app.billsplit.billsplit.DashBoardFeature.Adapters.BalanceExpandableListAdapter
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks
import java.util.ArrayList
import com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard.BalanceItem
import com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard.DebtorMonthlyOverview
import com.app.billsplit.billsplit.DashBoardFeature.ObservableExpandableListView.ObservableExpandableListView
import com.app.billsplit.billsplit.DashBoardFeature.R
import android.widget.ExpandableListView.OnChildClickListener
import android.widget.ExpandableListView.OnGroupExpandListener






open class BalanceFragment : Fragment(), BalanceExpandableListAdapter.BalanceAdapterCallback {

    private var mBalanceItemsToDisplay: ArrayList<DebtorMonthlyOverview> = arrayListOf()
    private var mListView: ObservableExpandableListView? = null
    private var mAdapter: BalanceExpandableListAdapter? = null
    private var mCurrentMonth: Int = 0
    var mCallBack: BalanceFragmentCallBack? = null

    interface BalanceFragmentCallBack {
        fun changeBalanceToBills(debtorId: String)
        fun updatePaymentsGraphForDebtor(debtorId: String, month: Int)
        fun updatePaymentsGraphForBill(debtorId: String, billId: String, month: Int)
        fun getCurrentSelectedDebtor() : String?
        fun getCurrentSelectedBill() : String?
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null){
            mBalanceItemsToDisplay = arguments.getSerializable(
                    MONTHLY_OVERVIEW_KEY) as ArrayList<DebtorMonthlyOverview>
            mCurrentMonth = arguments.getSerializable(
                    CURRENT_MONTH) as Int
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.dashboard_debtor_balance_fragment, container, false)

        val parentActivity = activity
        mListView = view.findViewById(R.id.scroll) as ObservableExpandableListView
        applyInitialListAdapter(mListView!!)
        mListView!!.setTouchInterceptionViewGroup(parentActivity.findViewById(R.id.parallax_container) as ViewGroup)

        if (parentActivity is ObservableScrollViewCallbacks) {
            mListView!!.setScrollViewCallbacks(parentActivity as ObservableScrollViewCallbacks)
        }
        mListView?.choiceMode = ListView.CHOICE_MODE_SINGLE;
        mListView?.setSelector(android.R.color.darker_gray);
        return view
    }

    private fun applyInitialListAdapter(listView: ExpandableListView) {
        mAdapter = BalanceExpandableListAdapter(activity, mBalanceItemsToDisplay, this)
        listView.setAdapter(mAdapter)
        addClickListenersToList(listView)
    }

    private fun addClickListenersToList(listView: ExpandableListView){
        // Listview on child click listener
        listView.setOnChildClickListener(OnChildClickListener { parent, v, groupPosition, childPosition, id ->
            updatePaymentsGraphForBill(mBalanceItemsToDisplay[groupPosition].debtorId!!, mBalanceItemsToDisplay[groupPosition].billsAndPayments[childPosition].billId!!)
            false
        })

        // Listview Group expanded listener
        listView.setOnGroupExpandListener(OnGroupExpandListener { groupPosition ->
            updatePaymentsGraphForDebtor(mBalanceItemsToDisplay[groupPosition].debtorId!!)
        })

        // Listview Group collapsed  listener
        listView.setOnGroupCollapseListener(ExpandableListView.OnGroupCollapseListener { groupPosition ->
/*
            updatePaymentsGraphForDebtor(mBalanceItemsToDisplay[groupPosition].debtorId!!)
*/
        })
    }

    //retrieve first debtor in list's id
    fun getFirstDebtorsId(): String? {
        return mBalanceItemsToDisplay[0].debtorId
    }

    fun updateAdapter(newModel: ArrayList<BalanceItem>){
       /* mAdapter?.clear()
        mAdapter?.addAll(newModel)*/
        mAdapter?.notifyDataSetChanged()
    }

    override fun updatePaymentsGraphForDebtor(debtorId: String) {
        var month = mCurrentMonth;
        if (mCallBack?.getCurrentSelectedDebtor() != debtorId){
            mCallBack?.updatePaymentsGraphForDebtor(debtorId, month)
        }
    }

    override fun updatePaymentsGraphForBill(debtorId: String, billId: String) {
        var month = mCurrentMonth
        if (mCallBack?.getCurrentSelectedBill() != billId){
            mCallBack?.updatePaymentsGraphForBill(debtorId, billId, month) 
        }
    }

    companion object {
        private const val MONTHLY_OVERVIEW_KEY = "monthly_overview_model"
        private const val CURRENT_MONTH = "current_month"
        //instantiate with api response model
        fun newInstance(responseModel: ArrayList<DebtorMonthlyOverview>, currentMonth: Int, dashboardCallBack: BalanceFragmentCallBack): BalanceFragment {
            val fragment = BalanceFragment()
            val bundle = Bundle()
            bundle.putSerializable(MONTHLY_OVERVIEW_KEY, responseModel)
            bundle.putSerializable(CURRENT_MONTH, currentMonth)
            fragment.mCallBack = dashboardCallBack
            fragment.arguments = bundle

            return fragment
        }
    }
}
