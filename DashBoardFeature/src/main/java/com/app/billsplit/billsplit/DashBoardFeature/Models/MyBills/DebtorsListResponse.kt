package com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills

import java.io.Serializable

data class DebtorsListResponse(val name: String, val phoneNo: String, val debtorId: String) : Serializable