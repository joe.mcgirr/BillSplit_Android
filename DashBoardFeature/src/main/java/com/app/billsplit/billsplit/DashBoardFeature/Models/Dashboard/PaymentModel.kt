package com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard

import org.joda.time.DateTime
import java.io.Serializable

open class Payment(paid: Double, paymentRef: String?, date: DateTime) : Serializable {
    val amountPaid: Double = paid
    val date = date
    val paymentReference = paymentRef
}