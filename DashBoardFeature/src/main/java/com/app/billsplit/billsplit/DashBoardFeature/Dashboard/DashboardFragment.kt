package com.app.billsplit.billsplit.DashBoardFeature.Dashboard

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.util.SparseArray
import android.view.*
import android.widget.FrameLayout
import android.widget.ListView
import android.widget.OverScroller
import com.app.billsplit.Ui.widget.SlidingTabLayout
import com.github.ksoichiro.android.observablescrollview.*
import com.nineoldandroids.view.ViewHelper
import com.github.mikephil.charting.charts.LineChart
import org.joda.time.DateTime
import kotlin.collections.ArrayList
import android.view.ViewGroup
import com.app.billsplit.billsplit.DashBoardFeature.Dashboard.Graph.XAxisValueFormatter
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.data.LineData
import com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard.*
import com.app.billsplit.billsplit.DashBoardFeature.R
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.XAxis
import kotlinx.android.synthetic.main.dashboard_parent_fragment.view.*
import kotlinx.android.synthetic.main.my_bills_parent_fragment.view.*
import org.apache.commons.lang3.SerializationUtils


class DashboardFragment : Fragment(), ObservableScrollViewCallbacks, BalanceFragment.BalanceFragmentCallBack {
    var name: String = "Monthly Outlook"
    private var listener: OnFragmentInteractionListener? = null
    private val MAX_TEXT_SCALE_DELTA = 0.3f
    private val INVALID_POINTER = -1

    private var mImageView: View? = null
    private var mOverlayView: View? = null
    private var mInterceptionLayout: TouchInterceptionFrameLayout? = null
    private var mPager: ViewPager? = null
    private var mPagerAdapter: NavigationAdapter? = null
    private var mVelocityTracker: VelocityTracker? = null
    private var mScroller: OverScroller? = null
    private var mBaseTranslationY: Float = 0.toFloat()
    private var mMaximumVelocity: Int = 0
    private var mActivePointerId = INVALID_POINTER
    private var mSlop: Int = 0
    private var mFlexibleSpaceHeight: Int = 0
    private var mTabHeight: Int = 0
    private var mScrolled: Boolean = false
    private var mActionBarSize: Int = 0
    private var mLineChart: LineChart? = null
    private var mDashboardResponseModel: ArrayList<MonthlyOverview> = arrayListOf()
    private var mCurrentSelectedDebtorId: String? = null
    private var mCurrentSelectedBillId: String? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActionBarSize = listener!!.getActionBarSize()
        if (savedInstanceState == null){
            //get api response model from args
            mDashboardResponseModel = arguments.getSerializable(
                    RESPONSEMODEL_KEY) as ArrayList<MonthlyOverview>
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dashboard_parent_fragment, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun getSupportFragmentManager() : FragmentManager
        fun getActionBarSize() : Int
        fun getScreenHeight(): Int
        fun removeActionBarShadow()
        fun displayActionBarShadow()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPagerAdapter = NavigationAdapter(childFragmentManager, mDashboardResponseModel, this)
        mPager = view!!.findViewById(R.id.pager) as ViewPager
        mPager!!.adapter = mPagerAdapter
        //Not sure why, but this stop fragment pages being overwritten...
        mPager!!.offscreenPageLimit = 6;
        mImageView = view!!.findViewById(R.id.image)
        mOverlayView = view!!.findViewById(R.id.overlay)
        // Padding for ViewPager must be set outside the ViewPager itself
        // because with padding, EdgeEffect of  ViewPager become strange.
        mFlexibleSpaceHeight = resources.getDimensionPixelSize(R.dimen.dashboard_graph_container_height)
        mTabHeight = resources.getDimensionPixelSize(R.dimen.tab_height)
        view!!.findViewById<FrameLayout>(R.id.pager_wrapper).setPadding(0, mFlexibleSpaceHeight, 0, 0)
        val slidingTabLayout = view!!.findViewById<SlidingTabLayout>(R.id.sliding_tabs)
        slidingTabLayout.setCustomTabView(R.layout.tab_indicator, android.R.id.text1)
        val tabSelectedColor = ContextCompat.getColor(context, com.app.billsplit.billsplit.R.color.colorAccent)
        slidingTabLayout.setSelectedIndicatorColors(tabSelectedColor)
        slidingTabLayout.setDistributeEvenly(false)
        slidingTabLayout.setViewPager(mPager)
        (slidingTabLayout.layoutParams as FrameLayout.LayoutParams).topMargin = mFlexibleSpaceHeight - mTabHeight

        val vc = ViewConfiguration.get(context)
        mSlop = vc.scaledTouchSlop
        mMaximumVelocity = vc.scaledMaximumFlingVelocity
        mInterceptionLayout = view!!.findViewById(R.id.parallax_container) as TouchInterceptionFrameLayout
        mInterceptionLayout!!.setScrollInterceptionListener(mInterceptionListener)
        mScroller = OverScroller(context)
        ScrollUtils.addOnGlobalLayoutListener(mInterceptionLayout!!) {
            // Extra space is required to move mInterceptionLayout when it's scrolled.
            // It's better to adjust its height when it's laid out
            // than to adjust the height when scroll events (onMoveMotionEvent) occur
            // because it causes lagging.
            // See #87: https://github.com/ksoichiro/Android-ObservableScrollView/issues/87
            val lp = mInterceptionLayout!!.layoutParams as FrameLayout.LayoutParams
            lp.height = listener!!.getScreenHeight() + mFlexibleSpaceHeight
            mInterceptionLayout!!.requestLayout()

            updateFlexibleSpace()
            configureGraphStyle()
            //set starting nav tab on the current month
            mPager!!.currentItem = NavigationAdapter.MONTHSPAN - 2
            //updatePaymentsGraphForDebtor(getFirstDebtorId()!!, mPager!!.currentItem)
        }
    }
    private fun getFirstDebtorId(): String?{
        val month = mPager!!.currentItem
        val currentBalanceFragment = mPagerAdapter!!.getRegisteredFragment(month)
        return currentBalanceFragment.getFirstDebtorsId()

    }

    override fun onScrollChanged(scrollY: Int, firstScroll: Boolean, dragging: Boolean) {
    }

    override fun onDownMotionEvent() {}

    override fun onUpOrCancelMotionEvent(scrollState: ScrollState) {

    }

    private val mInterceptionListener = object : TouchInterceptionFrameLayout.TouchInterceptionListener {
        override fun shouldInterceptTouchEvent(ev: MotionEvent, moving: Boolean, diffX: Float, diffY: Float): Boolean {
            if (!mScrolled && mSlop < Math.abs(diffX) && Math.abs(diffY) < Math.abs(diffX)) {
                // Horizontal scroll is maybe handled by ViewPager
                return false
            }

            val scrollable = getCurrentScrollable()
            if (scrollable == null) {
                mScrolled = false
                return false
            }

            // If interceptionLayout can move, it should intercept.
            // And once it begins to move, horizontal scroll shouldn't work any longer.
            val flexibleSpace = mFlexibleSpaceHeight - mTabHeight
            val translationY = ViewHelper.getTranslationY(mInterceptionLayout)
            val scrollingUp = 0 < diffY
            val scrollingDown = diffY < 0
            if (scrollingUp) {
                //at top of page
                if (translationY < 0) {
                    mScrolled = true
                    //listener!!.hideFab()
                    listener!!.displayActionBarShadow()
                    return true
                }
            } else if (scrollingDown) {
                //bottom of page
                if (-flexibleSpace < translationY) {
                    //listener!!.showFab()
                    //remove shadow from actionbar so slidingNav fits in underneath
                    listener!!.removeActionBarShadow()
                    mScrolled = true
                    return true
                }
            }
            mScrolled = false
            return false
        }

        override fun onDownMotionEvent(ev: MotionEvent) {
            mActivePointerId = ev.getPointerId(0)
            mScroller!!.forceFinished(true)
            if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain()
            } else {
                mVelocityTracker!!.clear()
            }
            mBaseTranslationY = ViewHelper.getTranslationY(mInterceptionLayout)
            mVelocityTracker!!.addMovement(ev)
        }

        override fun onMoveMotionEvent(ev: MotionEvent, diffX: Float, diffY: Float) {
            val flexibleSpace = mFlexibleSpaceHeight - mTabHeight
            val translationY = ScrollUtils.getFloat(ViewHelper.getTranslationY(mInterceptionLayout) + diffY, (-flexibleSpace).toFloat(), 0f)
            val e = MotionEvent.obtainNoHistory(ev)
            e.offsetLocation(0f, translationY - mBaseTranslationY)
            mVelocityTracker!!.addMovement(e)
            updateFlexibleSpace(translationY)
        }

        override fun onUpOrCancelMotionEvent(ev: MotionEvent) {
            mScrolled = false
            mVelocityTracker!!.computeCurrentVelocity(1000, mMaximumVelocity.toFloat())
            val velocityY = mVelocityTracker!!.getYVelocity(mActivePointerId).toInt()
            mActivePointerId = INVALID_POINTER
            mScroller!!.forceFinished(true)
            val baseTranslationY = ViewHelper.getTranslationY(mInterceptionLayout).toInt()

            val minY = -(mFlexibleSpaceHeight - mTabHeight)
            val maxY = 0
            mScroller!!.fling(0, baseTranslationY, 0, velocityY, 0, 0, minY, maxY)
            Handler().post { updateLayout() }
        }
    }

    private fun updateLayout() {
        var needsUpdate = false
        var translationY = 0f
        if (mScroller!!.computeScrollOffset()) {
            translationY = mScroller!!.currY.toFloat()
            val flexibleSpace = mFlexibleSpaceHeight - mTabHeight
            if (-flexibleSpace <= translationY && translationY <= 0) {
                needsUpdate = true
            } else if (translationY < -flexibleSpace) {
                translationY = (-flexibleSpace).toFloat()
                needsUpdate = true
            } else if (0 < translationY) {
                translationY = 0f
                needsUpdate = true
            }
        }

        if (needsUpdate) {
            updateFlexibleSpace(translationY)
            Handler().post { updateLayout() }
        }
    }

    private fun getCurrentScrollable(): Scrollable? {
        val fragment = getCurrentFragment() ?: return null
        fragment.view ?: return null
        return this.view!!.findViewById<ListView>(R.id.scroll) as Scrollable
    }

    private fun updateFlexibleSpace() {
        updateFlexibleSpace(ViewHelper.getTranslationY(mInterceptionLayout))
    }

    private fun updateFlexibleSpace(translationY: Float) {
        ViewHelper.setTranslationY(mInterceptionLayout, translationY)
        val minOverlayTransitionY = mActionBarSize - mOverlayView!!.height
        ViewHelper.setTranslationY(mImageView, ScrollUtils.getFloat(-translationY / 2, minOverlayTransitionY.toFloat(), 0f))

        // Change alpha of overlay
        val flexibleRange = mFlexibleSpaceHeight - mActionBarSize
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat(-translationY / flexibleRange, 0f, 1f))

   }

    private fun configureGraphStyle(){
        mLineChart = view!!.findViewById<LineChart?>(R.id.debtor_payments_chart)
        //set scaling to false
        mLineChart?.setScaleEnabled(false)

        setGraphXAxisStyle()
        setGraphYAxisStyle()

        //removing legend & description
        val legend = mLineChart?.legend
        legend?.isEnabled = false
        mLineChart?.description = null
        //TODO: If debtors exist show this, otherwise show, no data please as a debtor or something
        mLineChart?.setNoDataText(getResources().getString(R.string.no_data_text))
        mLineChart?.setNoDataTextColor(R.color.lightGrey)
    }

    private fun setGraphXAxisStyle(){
        val xAxis: XAxis? = mLineChart!!.xAxis
        xAxis?.setDrawAxisLine(false)
        //xAxis?.axisLineColor = ContextCompat.getColor(context, R.color.lightGrey)
        //has to minus to retrieve the week 1 label when calling getFormattedValue on formatter
        xAxis?.axisMinimum = -1f
        //change this dynamically depending on days in month
        xAxis?.axisMaximum = 31f
        xAxis?.granularity = 7f
        xAxis?.textColor = ContextCompat.getColor(context, R.color.graph_text)
        val weeks = arrayOf("WEEK 1", "WEEK 2", "WEEK 3", "WEEK 4")
        xAxis?.valueFormatter = XAxisValueFormatter(weeks)
        xAxis?.enableGridDashedLine(0.5f, 0.2f, 0f)
        xAxis?.gridColor = ContextCompat.getColor(context, R.color.lightGrey)
        xAxis?.setAvoidFirstLastClipping(true)
        xAxis?.xOffset = 50f

    }

    private fun setGraphYAxisStyle(){
        val leftAxis = mLineChart!!.axisLeft
        //leftAxis.axisMinimum = 0f
        leftAxis?.setDrawLabels(false)
        leftAxis?.setDrawAxisLine(false)
        leftAxis?.enableGridDashedLine(0.5f, 0.2f, 0f)
        leftAxis?.gridColor = ContextCompat.getColor(context, R.color.lightGrey);

        val rightAxis = mLineChart!!.axisRight
        rightAxis?.setDrawLabels(false)
        rightAxis?.setDrawAxisLine(false)
        rightAxis?.setDrawGridLines(false)
        rightAxis?.enableGridDashedLine(0.5f, 0.2f, 0f)
    }

    private fun populateDebtorPaymentsChart(payments: ArrayList<Payment>){
        // in this example, a LineChart is initialized from xml
        mLineChart = view!!.findViewById<LineChart?>(R.id.debtor_payments_chart)

        var totalPaid = 0.0
        var sortedPayments = arrayListOf<Payment>()
        sortedPayments.addAll(payments.sortedWith(compareBy(Payment::date)))

        //call function to retrieve from API here
        val entries: ArrayList<Entry> = ArrayList();
        for (payment in sortedPayments) {
            totalPaid += payment.amountPaid
            entries.add( Entry(payment.date.dayOfMonth.toFloat(), totalPaid.toFloat()));
        }
        //add payments past the visible view port on right and left axis to make it appear as tho the line continues infinitely
        entries.add(0, Entry(-6f, 0.0f))
        entries.add(Entry( 35f, 0.0f))

        val dataSet = LineDataSet(entries, "Payments")
        setDataSetStyling(dataSet)

        val lineData = LineData(dataSet)
        mLineChart!!.data = lineData
        //don't need to invalidate() when chart is animated
        mLineChart?.animateY(600, Easing.EasingOption.EaseInCubic)
        //Sets viewport to spread over the entire width of the screen
        // TODO: function required to set viewport scaling automagically based on screenSize
        mLineChart?.setViewPortOffsets(0f, 42f, 0f, 70f)

    }

    private fun setDataSetStyling(dataSet: LineDataSet){
        //sets color of line
        dataSet.color = ContextCompat.getColor(context, R.color.graph_text)
        dataSet.highLightColor = ContextCompat.getColor(context, R.color.white)
        dataSet.setCircleColor(R.color.colorAccent)
        dataSet.circleColors = listOf(ContextCompat.getColor(context, R.color.off_white))
        dataSet.lineWidth = 0.9f
        dataSet.setDrawFilled(true)
        dataSet.circleRadius = 7f
        dataSet.circleHoleRadius = 4f
        dataSet.setCircleColorHole(ContextCompat.getColor(context, R.color.colorAccent))
        dataSet.setDrawCircles(true)
        dataSet.mode = LineDataSet.Mode.CUBIC_BEZIER;
        dataSet.cubicIntensity = 0.1f
        dataSet.fillDrawable = ContextCompat.getDrawable(context, R.drawable.graph_gradient_background)
        dataSet.fillAlpha = 0
    }

    private fun getCurrentFragment(): Fragment? {
        return mPagerAdapter!!.getItemAt(mPager!!.currentItem)
    }

    override fun updatePaymentsGraphForDebtor(debtorId: String, month: Int) {
        mCurrentSelectedDebtorId = debtorId
        var debtorOverviewForMonth = getDebtorOverviewMatchingId(debtorId, mDashboardResponseModel[month].debtorMonthlyOverviews)
        var debtorBillsForMonth = debtorOverviewForMonth!!.billsAndPayments

        var debtorPayments = arrayListOf<Payment>()
        for(bill in debtorBillsForMonth){
            debtorPayments.addAll(bill.debtorsPayments as ArrayList<Payment>)
        }

        populateDebtorPaymentsChart(debtorPayments)
    }

    override fun updatePaymentsGraphForBill(debtorId: String, billId: String, month: Int) {
        mCurrentSelectedBillId = billId
        var debtorOverviewForMonth = getDebtorOverviewMatchingId(debtorId, mDashboardResponseModel[month].debtorMonthlyOverviews)
            var debtorBill = getBillOverviewMatchingId(billId, debtorOverviewForMonth!!.billsAndPayments)

        var billPayments = arrayListOf<Payment>()
        billPayments.addAll(debtorBill?.debtorsPayments as ArrayList<Payment>)

        populateDebtorPaymentsChart(billPayments)

    }

    override fun changeBalanceToBills(debtorId: String){
        for(month in (mPagerAdapter!!.mRegisteredFragments.size()-1).downTo(0)){
            val debtorOverview = getDebtorOverviewMatchingId(debtorId, this.mDashboardResponseModel[month].debtorMonthlyOverviews)
            val debtorsBills = debtorOverview?.billsAndPayments

            if (debtorsBills != null){
                //change balance fragment to display bills rather than debtors
                var monthsFragment = mPagerAdapter!!.getRegisteredFragment(month);
                monthsFragment.updateAdapter(debtorsBills as ArrayList<BalanceItem>)
            }
        }
    }

    override fun getCurrentSelectedDebtor(): String? {
        return mCurrentSelectedDebtorId
    }

    override fun getCurrentSelectedBill() : String? {
        return mCurrentSelectedBillId
    }


    private fun getDebtorOverviewMatchingId(debtorId: String, monthsDebtors: ArrayList<DebtorMonthlyOverview>) : DebtorMonthlyOverview?{
        for(debtor in monthsDebtors){
            if(debtor.debtorId == debtorId){
                return debtor;
            }
        }
        return null
    }


    private fun getBillOverviewMatchingId(billId: String, monthsBills: ArrayList<BillsMonthlyOverview>): BillsMonthlyOverview?{
        for(bill in monthsBills){
            if(bill.billId == billId){
                return bill;
            }
        }
        return null
    }

    companion object {
        private const val RESPONSEMODEL_KEY = "response_model"
        //instantiate with api response model
        fun newInstance(responseModel: ArrayList<MonthlyOverview>): DashboardFragment {
            return DashboardFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(RESPONSEMODEL_KEY, responseModel)
                }
            }
        }
    }

    private class NavigationAdapter : CacheFragmentStatePagerAdapter {
        var mRegisteredFragments = SparseArray<BalanceFragment>()
        var mDashboardModel: ArrayList<MonthlyOverview> = arrayListOf()
        var mCurrentFragment: DashboardFragment = DashboardFragment()

        constructor(fm: FragmentManager, dashboardModel: ArrayList<MonthlyOverview>, currentFragment: DashboardFragment) : super(fm){
            mCurrentFragment = currentFragment
            //create (deep) copy of API response, so it isn't changed when the adapters model is
            mDashboardModel = SerializationUtils.clone(dashboardModel)

        }

        override fun createItem(position: Int): Fragment {
            var f: BalanceFragment?
            val monthPosition = position % count
            var overviewModel = mDashboardModel!![monthPosition].debtorMonthlyOverviews
            f = BalanceFragment.newInstance(overviewModel, monthPosition, mCurrentFragment)
            return f
        }

        // Register the fragment when the item is instantiated - caches internally
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val fragment = super.instantiateItem(container, position) as BalanceFragment
            mRegisteredFragments.put(position, fragment)
            return fragment
        }

        // Unregister when the item is inactive
        override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any) {
            mRegisteredFragments.remove(position)
            super.destroyItem(container, position, `object`)
        }

        // Returns the fragment for the position (if instantiated)
        fun getRegisteredFragment(position: Int): BalanceFragment {
            return mRegisteredFragments.get(position)
        }

        override fun getCount(): Int {
            return TITLES.size
        }

        override fun getPageTitle(position: Int): CharSequence {
            return TITLES[position]
        }

        companion object {
            //This decides how many months to show on the dashboard
            val MONTHSPAN = 6
            val PREVIOUS_MONTHS = 4
            val FUTURE_MONTHS = 1
            fun getMonthNames(): ArrayList<String> {
                val monthRange = arrayListOf<String>()

                //5 to -1 to include the month after current month and the 6 before
                for (i in PREVIOUS_MONTHS downTo -FUTURE_MONTHS){
                    monthRange.add(DateTime().minusMonths(i).monthOfYear().asText)
                }
                return monthRange
            }

            private val TITLES = getMonthNames()
        }
    }

}
