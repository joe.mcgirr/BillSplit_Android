package com.app.billsplit.billsplit.DashBoardFeature.MyBills

import android.text.Editable
import android.text.TextWatcher

public class CurrencyTextWatcher : TextWatcher {
    override fun afterTextChanged(view: Editable) {
        var s: String? = null
        try {
            // The comma in the format specifier does the trick
            s = String.format("%,d", java.lang.Long.parseLong(view.toString()))
        } catch (e: NumberFormatException) {
        }
        // Set s back to the view after temporarily removing the text change listener
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}