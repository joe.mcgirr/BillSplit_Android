package com.app.billsplit.billsplit.DashBoardFeature.Adapters

import android.animation.ObjectAnimator
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.BaseExpandableListAdapter
import android.widget.ProgressBar
import android.widget.TextView
import com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard.BillsMonthlyOverview
import com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard.DebtorMonthlyOverview
import com.app.billsplit.billsplit.DashBoardFeature.R
import kotlinx.android.synthetic.main.dashboard_expandable_list_item.view.*


class BalanceExpandableListAdapter : BaseExpandableListAdapter{
    private var callBack: BalanceAdapterCallback? = null
    private var mContext:Context

    private lateinit var mlistDataHeader: ArrayList<DebtorMonthlyOverview>
    private lateinit var mlistDataChild: HashMap<DebtorMonthlyOverview, ArrayList<BillsMonthlyOverview>>
    var monthlyOverviews: ArrayList<DebtorMonthlyOverview>

    constructor(context: Context, singleMonthDashboardOverview: ArrayList<DebtorMonthlyOverview>, callingFragment: BalanceAdapterCallback?){
        this.callBack = callingFragment
        this.monthlyOverviews = singleMonthDashboardOverview
        this.mContext = context

        this.mlistDataHeader = singleMonthDashboardOverview
        this.mlistDataChild = HashMap()
        for (monthOverview in monthlyOverviews){
            mlistDataChild[monthOverview] = monthOverview.billsAndPayments
        }
    }

    interface BalanceAdapterCallback {
        fun updatePaymentsGraphForDebtor(debtorId: String)
        fun updatePaymentsGraphForBill(debtorId: String, billId: String)
    }

    override fun getChild(groupPosition: Int, childPosititon: Int): BillsMonthlyOverview {
        return mlistDataChild[mlistDataHeader[groupPosition]]!![childPosititon];
    }

    //replace with billId?
    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int,
                              isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        if (convertView == null) {
            val inflater = mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.dashboard_expandable_list_item, null)
        }
        val billName = convertView?.findViewById(R.id.debtor_name) as TextView
        val lastPayment = convertView?.findViewById(R.id.last_payment) as TextView
        val owed = convertView?.findViewById(R.id.total_owed) as TextView
        val paid = convertView?.findViewById(R.id.total_paid) as TextView

        billName.text = getChild(groupPosition, childPosition).name
        lastPayment.text = "last payment: 20/07/18"
        owed.text = "£"+ getChild(groupPosition, childPosition).totalOwed.toString() + "0"
        paid.text = "£"+ getChild(groupPosition, childPosition).totalPaid.toString() + "0"

        return convertView
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return mlistDataChild[mlistDataHeader[groupPosition]]!!.size
    }

    override fun getGroup(groupPosition: Int): DebtorMonthlyOverview {
        return this.mlistDataHeader[groupPosition]
    }

    override fun getGroupCount(): Int {
        return this.mlistDataHeader.size
    }

    //replace these with id of debtor?
    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                              convertView: View?, parent: ViewGroup): View {
        var convertView = convertView

        if (convertView == null) {
            val inflater = mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.dashboard_expandable_list_group, null)
        }

        val currentDebtor = getGroup(groupPosition)
        //get views
        val debtorName = convertView?.findViewById(R.id.debtor_name) as TextView
        val toPay = convertView?.findViewById(R.id.to_pay) as TextView
        val totalOwed = convertView?.findViewById<TextView>(R.id.total_owed_progress_text)
        //populate with model data
        debtorName.text = currentDebtor.name
        toPay.text = "To pay: £" + (currentDebtor!!.totalOwed - currentDebtor!!.totalPaid).toString()
        totalOwed.text = "£"+ currentDebtor.totalOwed.toString() + "0"

        //populate & animate progress
        val progressBar = convertView?.findViewById(R.id.progress_bar_foreground) as ProgressBar
        progressBar.progress = calculateProgress(currentDebtor.totalOwed, currentDebtor.totalPaid)
        animateProgressBar(progressBar)

        return convertView!!
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    private fun calculateProgress(totalOwed: Double, totalPayed: Double): Int {
        var maxCircleValue = 100;
        var circleSegmentSize = maxCircleValue / totalOwed;
        var segmentsToFill = circleSegmentSize * totalPayed
        return segmentsToFill.toInt()
    }

    private fun animateProgressBar(progressBar: ProgressBar){
        val animation = ObjectAnimator.ofInt(progressBar, "progress", 0, progressBar.progress) // see this max value coming back here, we animate towards that value
        animation.duration = 1550 // in milliseconds
        animation.interpolator = DecelerateInterpolator()
        animation.start()
    }
}