package com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard

import com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard.BalanceItem
import com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard.BillsMonthlyOverview
import java.io.Serializable

class DebtorMonthlyOverview(name: String, owed: Double, paid: Double, id: String) : BalanceItem(name, owed, paid, id, null), Serializable{
    var billsAndPayments: ArrayList<BillsMonthlyOverview> = arrayListOf()
}