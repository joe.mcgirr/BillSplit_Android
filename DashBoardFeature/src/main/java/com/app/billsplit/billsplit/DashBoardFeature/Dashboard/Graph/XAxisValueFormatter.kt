package com.app.billsplit.billsplit.DashBoardFeature.Dashboard.Graph

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import kotlin.math.ceil


class XAxisValueFormatter : IAxisValueFormatter {
    private var mValues: Array<String> = arrayOf()

    constructor(values: Array<String>) {
        this.mValues = values
    }

    override fun getFormattedValue(value: Float, axis: AxisBase): String {
        // "value" represents the position of the label on the axis (x or y)
        var weekNumber = ceil((value / 7)) - 1

        if (weekNumber <= 0) weekNumber = 0f
        //required for days over the 28th
        if (weekNumber >= 4) weekNumber = 3f

        return mValues[weekNumber.toInt()]
    }

    /** this is only needed if numbers are returned, else return 0  */
    fun getDecimalDigits(): Int {
        return 0
    }
}