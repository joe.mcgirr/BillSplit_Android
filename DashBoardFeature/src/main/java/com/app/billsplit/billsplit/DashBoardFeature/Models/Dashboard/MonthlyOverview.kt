package com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard

import com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard.DebtorMonthlyOverview
import org.joda.time.DateTime
import java.io.Serializable

data class MonthlyOverview(val date: DateTime,
                           var debtorMonthlyOverviews: ArrayList<DebtorMonthlyOverview>) : Serializable {

}