package com.app.billsplit.billsplit.DashBoardFeature.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


import com.app.billsplit.billsplit.DashBoardFeature.MyBills.MyBillsListFragment.OnListFragmentInteractionListener
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.BillsListResponse
import kotlinx.android.synthetic.main.my_bills_bills_list_item.view.*
import com.app.billsplit.billsplit.DashBoardFeature.R
import org.joda.time.format.DateTimeFormat

class MyBillsRecyclerViewAdapter(
        private val mBillsResponse: List<BillsListResponse>,
        private val mListener: OnListFragmentInteractionListener?)
    : RecyclerView.Adapter<MyBillsRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as BillsListResponse
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteractionBill(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.my_bills_bills_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mBillsResponse[position]
        holder.mBillName.text = item.name
        holder.mBillCost.text = "£" + item.cost.toString() + "0"
        holder.mBillCreated.text = item.created.toString(DateTimeFormat.shortDate())

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mBillsResponse.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mBillName: TextView = mView.bill_name
        val mBillCost: TextView = mView.amount_owed
        val mBillCreated: TextView = mView.created
    }
}
