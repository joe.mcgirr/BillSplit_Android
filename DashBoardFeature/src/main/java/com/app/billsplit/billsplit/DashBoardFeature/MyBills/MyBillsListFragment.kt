package com.app.billsplit.billsplit.DashBoardFeature.MyBills

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.billsplit.billsplit.DashBoardFeature.Adapters.MyBillsRecyclerViewAdapter
import com.app.billsplit.billsplit.DashBoardFeature.Models.MyBills.BillsListResponse
import com.app.billsplit.billsplit.DashBoardFeature.R
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

class MyBillsListFragment : Fragment() {

    private var mCallBack: OnListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //load bundle
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.my_bills_bills_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
                adapter = MyBillsRecyclerViewAdapter(getBillDummyData(), mCallBack)
                addItemDecoration(DividerItemDecoration(context, RecyclerView.VERTICAL))

            }
        }
        return view
    }

    override fun onDetach() {
        super.onDetach()
        mCallBack = null
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteractionBill(item: BillsListResponse)

    }


    fun getBillDummyData(): ArrayList<BillsListResponse> {
        val dtfOut: DateTimeFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
        return arrayListOf<BillsListResponse>(
                BillsListResponse("Water", DateTime.now(), 20.00),
                BillsListResponse("Leccy", DateTime.now(), 20.00),
                BillsListResponse("Tuition", DateTime.now(), 20.00),
                BillsListResponse("Broadband", DateTime.now(), 20.00),
                BillsListResponse("Tv License", DateTime.now(), 20.00),
                BillsListResponse("Whatever", DateTime.now(), 20.00)
        )
    }

    companion object {
        fun newInstance(listener: OnListFragmentInteractionListener): MyBillsListFragment {
            val fragment = MyBillsListFragment()
            val bundle = Bundle()
            //bundle.putSerializable(RESPONSEMODEL_KEY, )
            if (listener is OnListFragmentInteractionListener) {
                fragment.mCallBack = listener
            } else {
                throw RuntimeException(listener.toString() + " must implement OnListFragmentInteractionListener")
            }
            fragment.arguments = bundle

            return fragment
        }
    }
}
