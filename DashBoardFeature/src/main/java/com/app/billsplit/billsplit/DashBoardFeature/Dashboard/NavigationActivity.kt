package com.app.billsplit.billsplit.DashBoardFeature.Dashboard

import android.opengl.Visibility
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.activity_app_bar_navigation.*
import android.widget.Toast
import com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard.*
import com.app.billsplit.billsplit.DashBoardFeature.MyBills.*
import com.app.billsplit.billsplit.DashBoardFeature.R
import org.joda.time.DateTime
import java.util.*
import kotlin.collections.ArrayList


class NavigationActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, DashboardFragment.OnFragmentInteractionListener, MyBillsTabFragment.OnFragmentInteractionListener,
AddDebtorFragment.OnFragmentInteractionListener, AddBillFragment.OnFragmentInteractionListener, AddBillFragmentPage2.OnFragmentInteractionListener, NewOrOldDebtor.OnFragmentInteractionListener,
DebtorHowMuchToPay.OnFragmentInteractionListener{

    private var mDashboardResponseModel: ArrayList<MonthlyOverview> = arrayListOf<MonthlyOverview>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayShowTitleEnabled(false)

        mDashboardResponseModel = getDashboardApiResponseModel();

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        //if not restored from previous state
        if (savedInstanceState == null) {
            addInitialFragment();
        }

    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_dashboard -> {
                val dashboardFragment = DashboardFragment.newInstance(mDashboardResponseModel)
                replaceContentFragment(dashboardFragment, "Monthly Outlook");
            }
            R.id.nav_my_bills -> {
                val myBillsFragment = MyBillsTabFragment()
                replaceContentFragment(myBillsFragment, "My Bills")
            }
            R.id.nav_my_debts -> {
                val toast = Toast.makeText(applicationContext, "Not Implemented Yet", Toast.LENGTH_SHORT)
                toast.show()
            }
            R.id.nav_rate -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun getSupportFragmentManager() : FragmentManager {
        return super.getSupportFragmentManager()
    }

    override fun replaceContentFragment(newFragment: Fragment, title: String){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_frame_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
        toolbar_title.text = title;
    }

    fun addInitialFragment() {
        //make API call here to get the dashboard response object and pass into dashboard fragment
        if (fragment_frame_container != null) {
            val dashboardFragment = DashboardFragment.newInstance(mDashboardResponseModel);
            supportFragmentManager.beginTransaction()
                    .add(R.id.fragment_frame_container, dashboardFragment).commit()
            toolbar_title.text = "Monthly Outlook";
        }
    }

    companion object {

        fun getDashboardApiResponseModel() : ArrayList<MonthlyOverview>{

            return getDashboardDummyDate();

        }

        //1 debtor, 2 bills
        private fun getDashboardDummyDate(): ArrayList<MonthlyOverview> {
            val debtor_1_Id = "1916b24d-1a74-4e77-b41b-40ca1ddb44aa"
            val debtor_2_Id = "cd322cd0-4391-490e-a120-d534b6bc384c"

            val firstMonth = DateTime.now().minusMonths(6)
            val firstDayOfMonth = firstMonth.dayOfMonth().withMinimumValue()

            //water payments
            val waterDebtorPayments : ArrayList<DebtorPayment> = arrayListOf(
                    DebtorPayment(4.00, paymentRef = "XYWFHWY", date = firstDayOfMonth.plusDays(3)),
                    DebtorPayment(6.00, paymentRef = "XYWFHWY", date = firstDayOfMonth.plusDays(6)),
                    DebtorPayment(2.00, paymentRef = "XYWFHWY", date = firstDayOfMonth.plusDays(8)),
                    DebtorPayment(8.00, paymentRef = "XYWFHWY", date = firstDayOfMonth.plusDays(18))
            )
            val waterDebtorPayments2 : ArrayList<DebtorPayment> = arrayListOf(
                    DebtorPayment(10.00, paymentRef = "XYWFHWY", date = firstDayOfMonth.plusDays(12)),
                    DebtorPayment(5.00, paymentRef = "XYWFHWY", date = firstDayOfMonth.plusDays(16)),
                    DebtorPayment(3.00, paymentRef = "XYWFHWY", date = firstDayOfMonth.plusDays(20)),
                    DebtorPayment(7.00, paymentRef = "XYWFHWY", date = firstDayOfMonth.plusDays(26))
            )
            val waterBillPayments : ArrayList<BillPayment> = arrayListOf(
                    BillPayment(25.00, DateTime.now())
            )

            //energy payments
            val energyBillPayments: ArrayList<BillPayment> = arrayListOf(
                    BillPayment(25.00, DateTime.now().minusMonths(1)),
                    BillPayment(60.00, DateTime.now().minusMonths(1))
            )
            val energyDebtorPayments : ArrayList<DebtorPayment> = arrayListOf(
                    DebtorPayment(2.00, paymentRef = "YSFJKDD", date = firstDayOfMonth.plusDays(2)),
                    DebtorPayment(3.00, paymentRef = "YSFJKDD", date = firstDayOfMonth.plusDays(19)),
                    DebtorPayment(12.00, paymentRef = "YSFJKDD", date = firstDayOfMonth.plusDays(27)),
                    DebtorPayment(3.00, paymentRef = "YSFJKDD", date = firstDayOfMonth.plusDays(5))
            )

            val energyDebtorPayments2 : ArrayList<DebtorPayment> = arrayListOf(
                    DebtorPayment(20.00, paymentRef = "YSFJKDD", date = firstDayOfMonth.plusDays(4)),
                    DebtorPayment(5.00, paymentRef = "YSFJKDD", date = firstDayOfMonth.plusDays(9)),
                    DebtorPayment(12.00, paymentRef = "YSFJKDD", date = firstDayOfMonth.plusDays(23)),
                    DebtorPayment(3.00, paymentRef = "YSFJKDD", date = firstDayOfMonth.plusDays(5))
            )

            //DEBTOR 1
            val debtor_1_first_month = DebtorMonthlyOverview("Nuala Flynn", 20.00, 15.00, debtor_1_Id)
            val debtor_1_bills_overview = arrayListOf(
                    BillsMonthlyOverview("Water", UUID.randomUUID().toString(), debtor_1_Id, 10.00, 5.00, waterBillPayments, waterDebtorPayments),
                    BillsMonthlyOverview("Energy", UUID.randomUUID().toString(), debtor_1_Id, 10.00, 10.00, energyBillPayments, energyDebtorPayments)
            )
            debtor_1_first_month.billsAndPayments = debtor_1_bills_overview

            val debtor_1_second_month = DebtorMonthlyOverview("Nuala Flynn", 50.00, 14.00, debtor_1_Id)
            val debtor_1_bills_overview2 = arrayListOf(
                    BillsMonthlyOverview("Water", UUID.randomUUID().toString(), debtor_1_Id, 5.00, 2.58, waterBillPayments, waterDebtorPayments),
                    BillsMonthlyOverview("Energy", UUID.randomUUID().toString(), debtor_1_Id, 80.00, 10.00, energyBillPayments, energyDebtorPayments)
            )
            debtor_1_second_month.billsAndPayments = debtor_1_bills_overview2

            //DEBTOR 2
            val debtor_2_first_month = DebtorMonthlyOverview("Katie Botteril", 40.00, 3.00, debtor_2_Id)
            val debtor_2_bills_overview = arrayListOf(
                    BillsMonthlyOverview("Water", UUID.randomUUID().toString(), debtor_2_Id, 10.00, 5.00, waterBillPayments, waterDebtorPayments2),
                    BillsMonthlyOverview("Energy", UUID.randomUUID().toString(), debtor_2_Id, 10.00, 10.00, energyBillPayments, energyDebtorPayments2)
            )
            debtor_2_first_month.billsAndPayments = debtor_2_bills_overview

            val debtor_2_second_month = DebtorMonthlyOverview("Katie Botteril", 17.00, 14.00, debtor_2_Id)
            val debtor_2_bills_overview2 = arrayListOf(
                    BillsMonthlyOverview("Water", UUID.randomUUID().toString(), debtor_2_Id, 30.00, 13.58, waterBillPayments, waterDebtorPayments2),
                    BillsMonthlyOverview("Energy", UUID.randomUUID().toString(), debtor_2_Id, 8.00, 0.00, energyBillPayments, energyDebtorPayments2)
            )
            debtor_2_second_month.billsAndPayments = debtor_2_bills_overview2

            //OTHER STUFF
            val month_1_debtorMonthlyOverviews = arrayListOf<DebtorMonthlyOverview>()

            val month_2_debtorMonthlyOverviews = arrayListOf<DebtorMonthlyOverview>()

            val month_3_debtorMonthlyOverviews = arrayListOf<DebtorMonthlyOverview>()
            val month_4_debtorMonthlyOverviews = arrayListOf(
                    debtor_1_first_month,
                    debtor_2_first_month
            )
            val month_5_debtorMonthlyOverviews = arrayListOf(
                    debtor_1_second_month,
                    debtor_2_second_month)
            val month_6_debtorMonthlyOverviews = arrayListOf<DebtorMonthlyOverview>()


            return arrayListOf(
                    MonthlyOverview(DateTime.now().minusMonths(4), month_1_debtorMonthlyOverviews),
                    MonthlyOverview(DateTime.now().minusMonths(3), month_2_debtorMonthlyOverviews),
                    MonthlyOverview(DateTime.now().minusMonths(2), month_3_debtorMonthlyOverviews),
                    MonthlyOverview(DateTime.now().minusMonths(1), month_4_debtorMonthlyOverviews),
                    MonthlyOverview(DateTime.now().minusMonths(0), month_5_debtorMonthlyOverviews),
                    MonthlyOverview(DateTime.now().plusMonths(1), month_6_debtorMonthlyOverviews)
            )
        }
    }

    override fun getActionBarSize(): Int {
        val typedValue = TypedValue()
        val textSizeAttr = intArrayOf(android.support.v7.appcompat.R.attr.actionBarSize)
        val indexOfAttrTextSize = 0
        val a = obtainStyledAttributes(typedValue.data, textSizeAttr)
        val actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1)
        a.recycle()
        return actionBarSize
    }

    override fun getScreenHeight(): Int {
        return findViewById<View>(android.R.id.content).height
    }

    override fun removeActionBarShadow() {
        findViewById<View>(R.id.toolbar_shadow).visibility = View.GONE
    }

    override fun displayActionBarShadow(){
        findViewById<View>(R.id.toolbar_shadow).visibility = View.VISIBLE
    }


/*
    override fun showFab() {
        if (!mFabIsShown) {
            ViewPropertyAnimator.animate(mFab).cancel()
            ViewPropertyAnimator.animate(mFab).scaleX(1f).scaleY(1f).setDuration(200).start()
            mFabIsShown = true
        }
    }

    override fun hideFab() {
        if (mFabIsShown) {
            ViewPropertyAnimator.animate(mFab).cancel()
            ViewPropertyAnimator.animate(mFab).scaleX(0f).scaleY(0f).setDuration(200).start()
            mFabIsShown = false
        }
    }
*/


}
