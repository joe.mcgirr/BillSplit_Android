package com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard

import org.joda.time.DateTime
import java.io.Serializable

class BillPayment(paid: Double, date: DateTime) : Payment(paid, null, date), Serializable{
}