package com.app.billsplit.billsplit.DashBoardFeature.Models.Dashboard

import org.joda.time.DateTime
import java.io.Serializable

class DebtorPayment(paid: Double, paymentRef: String, date: DateTime) : Payment(paid, paymentRef, date), Serializable{

}