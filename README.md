BillSplit Android Mobile App: User Guide

To run:

•	The app can either be run in debug mode or installing the billSplitAndroid.apk APK file directly onto an emulator/mobile.

•	To run in debug mode, open the BillSplit_Android in android studio, 

•	ensure to select the app folder from the dropdown menu next to the Android Studio run button.

•	When the app startups a login page will show use the credentials:

        User: user1@billsplit.com
        
        Pass: gpwd_2015!?
        
•	This will connect with the monolith running on an azure server and open the main dashboard to the app.


Prequisites

•	Android studio and suitable device to run on.

File Structure

•	Most of the code for the project is in the Dashboard Feature Folder

•	Billsplit.DashboardFeature: This is where most of the Kotlin code is for the logic:

    o	Adapters: all adapters used in proj
    
    o	Dashboard: the dashboard workflow (see thesis for more info)
    
        -	NavigationActivity is the parent activity, replaces fragment_frame_container with different fragments, depending on what’s selected from side menu.
        
        -	Fragments can also callback to the navigation activity to swap the fragment, for example on a button click.
        
o	Models: Models used throughout

o	MyBills: Workflows consisting of adding a new bill and viewing current bills and debtors (see thesis for more information)

o	ObservableExpandableListView provides a nice view of the debtor payments that slides into the navbar on scroll up.

o	Retrofit: used to integrate the app with the backend api. Only used in login screen currently. 